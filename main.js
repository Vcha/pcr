//fonction pour le menu responsive
function myFunction() {
	var x = document.getElementById("myTopnav");
	if (x.className === "topnav") {
		x.className += " responsive";
	} else {
		x.className = "topnav";
	}
}

//Code pour l'affichage du titre lettre par lettre
var chaine = "LE PETIT CHAPERON ROUGE";
var nb_car = chaine.length;
var tableau = chaine.split("");
texte = new Array;
var txt = '';
var nb_msg = nb_car - 1;
for (i=0; i<nb_car; i++) {
	texte[i] = txt+tableau[i];
	var txt = texte[i];
}

actual_texte = 0;
function changeMessage(){
	document.getElementById("lettres").innerHTML = texte[actual_texte];
	actual_texte++;
	if(actual_texte >= texte.length){
		actual_texte = nb_msg;
	}
}
if(document.getElementById){
	setInterval("changeMessage()",100);
}




//évènement click pour changer la morale
var btn_change_morale = document.getElementById('btn-changez-la-morale');
var div_parent = document.querySelector('div.centrer-texte.divParent');

btn_change_morale.addEventListener('click', changerMorale);
function changerMorale(){

	btn_change_morale.style.display="none";
	var para_morale = document.querySelector('p.morale');
	para_morale.remove();

	var nvelle_morale = document.createElement('textarea');
	nvelle_morale.classList.add('textarea');
	nvelle_morale.innerHTML = para_morale.innerHTML;


	var btn_valider =  document.createElement('button');
	btn_valider.innerHTML = "Valider";
	btn_valider.classList.add('btnMoraleValidation');

	div_parent.appendChild(nvelle_morale);
	div_parent.appendChild(btn_valider);

	btn_valider.addEventListener('click', ()=>{
		btn_change_morale.style.display="block";
		var texte = nvelle_morale.value;
		nvelle_morale.remove();
		btn_valider.remove();

		var para = document.createElement('p');
		para.classList.add('morale');
		para.innerHTML = texte;
		div_parent.appendChild(para);
	});


}

//code pour faire apparaitre le texte en écoutant le scroll
var premierPara = document.querySelector('div.prems');
var deuxiemePara = document.querySelector('div.deuse');
var troisiemePara = document.querySelector('div.troise');
var morale = document.querySelector('div.divParent');

window.addEventListener('scroll', ()=>{
	if(window.scrollY>300){
		premierPara.classList.add('active');

	}else{
		premierPara.classList.remove('active');
	}
});

window.addEventListener('scroll', ()=>{
	if(window.scrollY>1200){
		deuxiemePara.classList.add('active');

	}else{
		deuxiemePara.classList.remove('active');
	}
});

window.addEventListener('scroll', ()=>{
	if(window.scrollY>2100){
		troisiemePara.classList.add('active');

	}else{
		troisiemePara.classList.remove('active');
	}
});

window.addEventListener('scroll', ()=>{
	if(window.scrollY>3000){
		morale.classList.add('active');

	}else{
		morale.classList.remove('active');
	}
});



//Code pour changer l'image
var pageDeGarde = document.getElementById('page-de-garde');
var changerImage = document.getElementById('changerImage');
var tcheckUn = 1;

changerImage.addEventListener('click', ()=>{
	if(tcheckUn == 0){
		pageDeGarde.style.backgroundImage = "url('1.jpg')";
		tcheckUn = tcheckUn + 1;
	} else if(tcheckUn == 1){
		pageDeGarde.style.backgroundImage = "url('2.jpeg')";
		tcheckUn = tcheckUn - 1;
	}

});


//code pour passer en mode sombre
var contenu = document.querySelector('div.leConte');
var modeSombre = document.getElementById('modeSombre');
var boutonMorale = document.querySelector('div.choix button');
var lienRemonter = document.querySelector('div.choix a');
var tcheckDeux = 1;

modeSombre.addEventListener('click', ()=>{
	if(tcheckDeux == 0){
		contenu.style.background = "white";
		contenu.style.color = "black";
		lienRemonter.style.color = "black";
		lienRemonter.style.border = "solid black";
		tcheckDeux = tcheckDeux + 1;
	} else if(tcheckDeux == 1){
		contenu.style.background = "black";
		contenu.style.color = "white";
		boutonMorale.style.border = "solid 1px White"
		lienRemonter.style.color = "white";
		lienRemonter.style.border = "solid white";
		tcheckDeux = tcheckDeux - 1;
	}
});


//code pour changer la police
var body = document.querySelector('body');
var changerPolice = document.getElementById('changerPolice');
var tcheckTrois = 1;

changerPolice.addEventListener('click', ()=>{
	if(tcheckTrois == 0){
		body.style.fontFamily = "Kaushan Script, cursive";
		tcheckTrois = tcheckTrois + 1;
	} else if(tcheckTrois == 1){
		body.style.fontFamily = "New Tegomin, serif";
		tcheckTrois = tcheckTrois - 1;
	}
});

//code pour changer la taille de la police
var changerTaillePolice = document.getElementById('changerTaillePolice');
var tcheckQuatre = 1;

changerTaillePolice.addEventListener('click', ()=>{
	if(tcheckQuatre == 0){
		contenu.style.fontSize = "1em";
		tcheckQuatre = tcheckQuatre + 1;
	} else if(tcheckQuatre == 1){
		contenu.style.fontSize = "1.2em";
		tcheckQuatre = tcheckQuatre - 1;
	}
});
